import * as plugins from './smartgit.plugins.js';
import { GitRepo } from './smartgit.classes.gitrepo.js';

export class Smartgit {
  public smartenvInstance = new plugins.smartenv.Smartenv();
  public envDeps: {
    fs: any;
    http: any;
  } = {
    fs: null,
    http: null,
  };

  constructor() {}

  public async init() {
    if (this.smartenvInstance.isNode) {
      this.envDeps.fs = await this.smartenvInstance.getSafeNodeModule('fs');
      this.envDeps.http = await this.smartenvInstance.getSafeNodeModule(
        'isomorphic-git/http/node/index.js'
      );
    } else {
      throw new Error('currently only node.js is supported.');
    }
  }

  public async createRepoByClone(fromUrlArg: string, toDirArg: string) {
    const repo = await GitRepo.fromCloningIntoDir(this, fromUrlArg, toDirArg);
  }

  public async createRepoByInit(dirArg: string) {
    const repo = await GitRepo.fromCreatingRepoInDir(this, dirArg);
    return repo;
  }

  public async createRepoByOpen(dirArg: string) {
    const repo = await GitRepo.fromOpeningRepoDir(this, dirArg);
    return repo;
  }
}
