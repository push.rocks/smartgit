/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartgit',
  version: '3.0.0',
  description: 'smart wrapper for nodegit'
}
